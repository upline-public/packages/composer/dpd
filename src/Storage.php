<?php

namespace Uplinestudio\DpdModule;

use Error;
use Monolog\Handler\FirePHPHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Uplinestudio\DpdModule\Dto\StorageDto;

class Storage
{
    private static StorageDto $storageDto;
    private static Logger $logger;

    private function __construct()
    {
    }

    public static function init(StorageDto &$storageDto)
    {
        if (!empty(self::$storageDto)) {
            throw new Error('Storage data already inited!');
        }

        self::$logger = new Logger('main');
        self::$logger->pushHandler(new StreamHandler(__DIR__ . '/../storage/' . date('Y-m-d') . '.log', Logger::DEBUG));
        // TODO: check expired logs
        self::$logger->pushHandler(new FirePHPHandler());

        self::$storageDto = &$storageDto;
    }

    public static function getStorageDto(): StorageDto
    {
        return self::$storageDto;
    }

    public static function getLogger(): Logger
    {
        return self::$logger;
    }
}
