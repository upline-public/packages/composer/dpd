<?php

namespace Uplinestudio\DpdModule\Dto;

class StorageDto
{
    private string $delisId;
    private string $password;
    private string $messageLanguage;
    private string $token;
    private string $url;

    public function __construct(
        string $delisId,
        string $password,
        string $messageLanguage,
        string $url
    )
    {
        $this->delisId = $delisId;
        $this->password = $password;
        $this->messageLanguage = $messageLanguage;
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @param string $token
     */
    public function setToken(string $token): void
    {
        $this->token = $token;
    }

    /**
     * @return string
     */
    public function getDelisId(): string
    {
        return $this->delisId;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @return string
     */
    public function getMessageLanguage(): string
    {
        return $this->messageLanguage;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }
}
