<?php

namespace Uplinestudio\DpdModule\Dto;

class OrderDto
{
    private int $orderId;
    private string $firstname;
    private string $shippingAddress;
    private string $shippingCountryCode;
    private string $shippingPostcode;
    private string $shippingCity;
    private float $total;
    private string $currency;
    private string $email;
    private string $language_code;
    private string $lastname;
    private string $shippingHouse;
    private float $weight;
    private string $shortProductNames;
    private ?string $phone;

    public function __construct(
        int $orderId,
        string $firstname,
        string $lastname,
        string $shippingAddress,
        string $shippingCountryCode,
        string $shippingPostcode,
        string $shippingCity,
        string $shippingHouse,
        float $total,
        string $currency,
        string $email,
        string $language_code,
        int $weight,
        string $shortProductNames,
        ?string $phone = null
    )
    {
        $this->orderId = $orderId;
        $this->firstname = $firstname;
        $this->shippingAddress = $shippingAddress;
        $this->shippingCountryCode = $shippingCountryCode;
        $this->shippingPostcode = $shippingPostcode;
        $this->shippingCity = $shippingCity;
        $this->total = $total;
        $this->currency = $currency;
        $this->email = $email;
        $this->language_code = $language_code;
        $this->lastname = $lastname;
        $this->shippingHouse = $shippingHouse;
        $this->weight = $weight;
        $this->shortProductNames = $shortProductNames;
        $this->phone = $phone;
    }

    /**
     * @return int
     */
    public function getOrderId(): int
    {
        return $this->orderId;
    }

    /**
     * @return string
     */
    public function getFirstname(): string
    {
        return $this->firstname;
    }

    /**
     * @return string
     */
    public function getShippingAddress(): string
    {
        return $this->shippingAddress;
    }

    /**
     * @return string
     */
    public function getShippingCountryCode(): string
    {
        return $this->shippingCountryCode;
    }

    /**
     * @return string
     */
    public function getShippingPostcode(): string
    {
        return $this->shippingPostcode;
    }

    /**
     * @return string
     */
    public function getShippingCity(): string
    {
        return $this->shippingCity;
    }

    /**
     * @return float
     */
    public function getTotal(): float
    {
        return $this->total;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getLanguageCode(): string
    {
        return $this->language_code;
    }

    /**
     * @return string
     */
    public function getLastname(): string
    {
        return $this->lastname;
    }

    /**
     * @return string
     */
    public function getShippingHouse(): string
    {
        return $this->shippingHouse;
    }

    /**
     * @return float
     */
    public function getWeight(): int
    {
        return $this->weight;
    }

    /**
     * @return string
     */
    public function getShortProductNames(): string
    {
        return $this->shortProductNames;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }
}
