<?php

namespace Uplinestudio\DpdModule;

use Exception;
use Uplinestudio\DpdModule\Dto\OrderDto;
use Uplinestudio\DpdModule\Dto\StorageDto;
use Uplinestudio\DpdModule\Services\ShipmentService;

class DpdModule
{
    /**
     * @throws Exception
     */
    public function __construct(StorageDto $storageDto)
    {
        Storage::init($storageDto);

        $token = (new DpdAuth())->getToken();

        if (!empty($token)) {
            $storageDto->setToken($token);
        }
    }

    /**
     * @throws Exception
     */
    public function storeOrder(OrderDto $orderDto): object
    {
        return (new ShipmentService())->storeOrders($orderDto);
    }
}
