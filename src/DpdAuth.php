<?php

namespace Uplinestudio\DpdModule;

use Exception;
use Uplinestudio\DpdModule\Services\LoginService;

class DpdAuth
{
    private string $tokenPath;

    public function __construct()
    {
        $this->tokenPath = dirname(__DIR__) . '/storage/token';
    }

    /**
     * @throws Exception
     */
    public function getToken(): string
    {
        if (!is_file($this->tokenPath)) {
            file_put_contents($this->tokenPath, '');
        }

        $token = file_get_contents($this->tokenPath);

        if (empty($token) || $this->itIsTimeToChangeToken()) {
            $response = (new LoginService())->getAuth();

            $token = $response->return->authToken;

            file_put_contents($this->tokenPath, $token);
        }

        return $token;
    }

    private function itIsTimeToChangeToken(): bool
    {
        $lastModified = filemtime($this->tokenPath);
        $timeToCreateNewToken = strtotime(date("Y:m:d") . " 03:01:00");

        return $lastModified < $timeToCreateNewToken;
    }
}
