<?php

namespace Uplinestudio\DpdModule\Services;

use Exception;
use Uplinestudio\DpdModule\Dto\OrderDto;
use Uplinestudio\DpdModule\Storage;
use SoapClient;
use SoapHeader;

class ShipmentService extends SoapClient
{
    private const URL = 'ShipmentService/V4_4/';

    private const PRINT_OPTION = [
        'outputFormat' => 'PDF',
        'paperFormat' => 'A6',
    ];
    private const SENDING_DEPOT = '0104';
    private const PRODUCT_TYPE = 'CL';
    private const MPS_COMPLETE_DELIVERY = '0';
    private const SENDER_DATA = [
        'name1' => 'EscapeWelt GmbH',
        'street' => 'Angerstr. 1',
        'country' => 'DE',
        'zipCode' => '04827',
        'city' => 'Gerichshain',
        'customerNumber' => '4934124874904',
    ];
    private const ORDER_TYPE = 'consignment';
    private const PREDICT_CHANNEL = 1;

    public function __construct()
    {
        $storageDto = Storage::getStorageDto();

        parent::__construct($storageDto->getUrl() . self::URL . '?wsdl', ['trace' => 1]);
    }

    private function getHeader(): SoapHeader
    {
        $storageDto = Storage::getStorageDto();

        return new SoapHeader("http://dpd.com/common/service/types/Authentication/2.0", 'authentication', [
            'delisId' => $storageDto->getDelisId(),
            'authToken' => $storageDto->getToken(),
            'messageLanguage' => $storageDto->getMessageLanguage(),
        ]);
    }

    private function getRecipientDataArray(OrderDto $orderDto): array
    {
        $recipient = [
            'name1' => $orderDto->getFirstname(),
            'name2' => $orderDto->getLastname(),
            'street' => $orderDto->getShippingAddress(),
            'country' => $orderDto->getShippingCountryCode(),
            'zipCode' => $orderDto->getShippingPostcode(),
            'city' => $orderDto->getShippingCity(),
            'houseNo' => $orderDto->getShippingHouse()
        ];

        $phone = $orderDto->getPhone();

        if ($phone) {
            $recipient['phone'] = $phone;
        }

        return $recipient;
    }

    /**
     * @throws Exception
     */
    public function storeOrders(OrderDto $orderDto): object
    {
        $this->__setSoapHeaders($this->getHeader());

        $requestData = [
            'storeOrders' => [
                'printOptions' => [
                    'printOption' => self::PRINT_OPTION
                ],
                'order' => [
                    'generalShipmentData' => [
                        'identificationNumber' => (string)$orderDto->getOrderId(),
                        'sendingDepot' => self::SENDING_DEPOT,
                        'product' => self::PRODUCT_TYPE,
                        'mpsCompleteDelivery' => self::MPS_COMPLETE_DELIVERY,
                        'sender' => self::SENDER_DATA,
                        'recipient' => $this->getRecipientDataArray($orderDto),
                    ],
                    'parcels' => [
                        'parcelLabelNumber' => null,
                        'weight' => $orderDto->getWeight(),
                        'customerReferenceNumber1' => $orderDto->getOrderId(),
                        'customerReferenceNumber2' => substr($orderDto->getShortProductNames(), 0, 35),
                    ],
                    'productAndServiceData' => [
                        'orderType' => self::ORDER_TYPE,
                        'predict' => [
                            'channel' => self::PREDICT_CHANNEL,
                            'value' => $orderDto->getEmail(),
                            'language' => $orderDto->getLanguageCode(),
                        ],
                    ],
                ]
            ],
        ];

        Storage::getLogger()->info("storeOrders request data", $requestData);

        try {
            $response = $this->__soapCall('storeOrders', $requestData);
        } catch (Exception $exception) {
            throw new Exception(
                "Some error while soapCall('storeOrders'). \r\n" .
                    "Request: \r\n" . print_r($this->__getLastRequest(), true) . "\r\n" .
                    "Response: \r\n" . print_r($this->__getLastResponse(), true),
                500,
                $exception
            );
        }

        return $response;
    }
}
