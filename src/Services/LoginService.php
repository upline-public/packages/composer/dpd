<?php

namespace Uplinestudio\DpdModule\Services;

use Exception;
use Uplinestudio\DpdModule\Storage;
use SoapClient;

class LoginService extends SoapClient
{
    const URL = 'LoginService/V2_0/';

    public function __construct()
    {
        $storageDto = Storage::getStorageDto();

        parent::__construct($storageDto->getUrl() . self::URL . '?wsdl', ['trace' => 1]);
    }

    /**
     * @throws Exception
     */
    public function getAuth(): object
    {
        $storageDto = Storage::getStorageDto();
        try {
            $response = $this->__soapCall('getAuth', [
                'getAuth' => [
                    "delisId" => $storageDto->getDelisId(),
                    "password" => $storageDto->getPassword(),
                    "messageLanguage" => $storageDto->getMessageLanguage(),
                ]
            ]);
        } catch (Exception $exception) {
            throw new Exception(
                "Some error while soapCall('getAuth'). \r\n" .
                "Request: \r\n" . print_r($this->__getLastRequest(), true) . "\r\n" .
                "Response: \r\n" . print_r($this->__getLastResponse(), true),
                500,
                $exception
            );
        }

        return $response;
    }
}
