<?php

use Uplinestudio\DpdModule\DpdAuth;
use Uplinestudio\DpdModule\DpdModule;
use Uplinestudio\DpdModule\Dto\OrderDto;
use Uplinestudio\DpdModule\Dto\StorageDto;

require 'vendor/autoload.php';

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

$dpdAuth = new DpdAuth();

try {
    $dpdModule = new DpdModule(new StorageDto($_ENV['DPD_LOGIN'], $_ENV['DPD_PASSWORD'], $_ENV['DPD_LANGUAGE'], $_ENV['DPD_URL']));

    $response = $dpdModule->storeOrder(new OrderDto(
        '3',
        'Test firstname',
        'test lastname',
        'address',
        'PL',
        '12-123',
        'Poland city',
        '49',
        '12.23',
        'EUR',
        'test@test.test',
        'EN',
        '300',
        '1234512345123451234512345123451234512345',
        '9999999999'
    ));
} catch (Exception $e) {
    var_dump("Test error!", $e);
}

if (!empty($response->orderResult->output->content)) {
    file_put_contents('./storage/pdf.pdf', $response->orderResult->output->content);
    unset($response->orderResult->output->content);
}

var_dump("Test response", $response ?? null);

/*
object(stdClass)#31 (1) {
  ["orderResult"]=>
  object(stdClass)#29 (2) {
    ["output"]=>
    object(stdClass)#28 (1) {
      ["format"]=>
      string(3) "PDF"
      ["output"]=> string() "PDF STRING"
    }
    ["shipmentResponses"]=>
    object(stdClass)#18 (3) {
      ["identificationNumber"]=>
      string(1) "1"
      ["mpsId"]=>
      string(25) "MPS0998505325360720210818"
      ["parcelInformation"]=>
      object(stdClass)#15 (1) {
        ["parcelLabelNumber"]=>
        string(14) "09985053253607"
      }
    }
  }
}
 */
